#version 330 core

in vec3 posWorld;

out vec4 FragColor;

void main()
{
	if(posWorld.y < 0)
		FragColor = vec4(0.0, 0.0, 1.0, 1.0);
	else
		FragColor = vec4(0.0, 1.0, 0.0, 1.0);
}