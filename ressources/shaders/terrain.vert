#version 330 core

layout (location = 0) in vec3 pos;

uniform mat4 modelViewProjectionMatrix;

out vec3 posWorld;

void main()
{
	posWorld = pos;
    gl_Position = modelViewProjectionMatrix * vec4(pos, 1.0);
}