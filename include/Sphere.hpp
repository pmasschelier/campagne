#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "OGLWorld.hpp"

class Sphere {

public:
    /* \brief Calcule le tableau de sommets et d'indices pour une sphère
     * \param meridians nombre de méridiens souhaités pour la sphère
     * \param parallels nombre de parallèles souhaités pour la sphère
     * \param vertices tableau de vertices à remplir
     * \param elements tableau d'indices à remplir
     */
    static void computeVertices(unsigned short meridians, unsigned short parallels, std::vector<float>& vertices, std::vector<unsigned int>& elements);
};

#endif