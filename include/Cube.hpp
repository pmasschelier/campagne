#ifndef CUBE_HPP
#define CUBE_HPP

#include "OGLWorld.hpp"

class Cube {

public:

/* ordre : +x, -x, +y, -y, +z, -z, le premier triangle est toujours celui situé le plus vers -x, -y 
 *  les triangles sont tracés sans passer par l'hypothénus et dans le sens trigo.
 *  on a touours un triangle en bas à droite et un triangle en bas à gauche
 */

	static constexpr std::array<float, 108> CUBE = 
	{
		1.0, -1.0, 1.0,     1.0, -1.0, -1.0,     1.0, 1.0, -1.0,		// Face gauche (+x)
		1.0, 1.0, -1.0,		1.0, 1.0, 1.0,       1.0, -1.0, 1.0,		// Face gauche (+x)

		-1.0, -1.0, -1.0,	-1.0, -1.0, 1.0,	-1.0, 1.0, 1.0,			// Face droite (-x)
		-1.0, 1.0, 1.0,		-1.0, 1.0, -1.0,    -1.0, -1.0, -1.0, 		// Face droite (-x)

		1.0, 1.0, -1.0,     -1.0, 1.0, -1.0,     -1.0, 1.0, 1.0, 		// Face dessus (+y)
		-1.0, 1.0, 1.0,		1.0, 1.0, 1.0,		1.0, 1.0, -1.0,			// Face dessus (+y)

		-1.0, -1.0, 1.0,   -1.0, -1.0, -1.0,	1.0, -1.0, -1.0,		// Face dessous (-y)
		1.0, -1.0, -1.0,	1.0, -1.0, 1.0,     -1.0, -1.0, 1.0,	    // Face dessous (-y)

		-1.0, -1.0, 1.0,	1.0, -1.0, 1.0,		1.0, 1.0, 1.0,			// Face arrière (+z)
		1.0, 1.0, 1.0,		-1.0, 1.0, 1.0,     -1.0, -1.0, 1.0,  		// Face arrière (+z)

		1.0, -1.0, -1.0,	-1.0, -1.0, -1.0,   -1.0, 1.0, -1.0,        // Face avant (-z)
		-1.0, 1.0, -1.0,    1.0, 1.0, -1.0,     1.0, -1.0, -1.0         // Face avant (-z)
	};


	static constexpr std::array<float, 72> VERTICES =
	{
		1.0, -1.0, 1.0,     1.0, -1.0, -1.0,     1.0, 1.0, -1.0,	1.0, 1.0, 1.0,		// Face gauche (+x)
		-1.0, -1.0, -1.0,	-1.0, -1.0, 1.0,	-1.0, 1.0, 1.0,		-1.0, 1.0, -1.0,	// Face droite (-x)
		1.0, 1.0, -1.0,     -1.0, 1.0, -1.0,     -1.0, 1.0, 1.0,	1.0, 1.0, 1.0, 		// Face dessus (+y)
		-1.0, -1.0, 1.0,   -1.0, -1.0, -1.0,	1.0, -1.0, -1.0,	1.0, -1.0, 1.0,		// Face dessous (-y)
		-1.0, -1.0, 1.0,	1.0, -1.0, 1.0,		1.0, 1.0, 1.0,		-1.0, 1.0, 1.0,		// Face arrière (+z)
		1.0, -1.0, -1.0,	-1.0, -1.0, -1.0,   -1.0, 1.0, -1.0,	1.0, 1.0, -1.0		// Face avant (-z)
	};

	static constexpr std::array<float, 72> NORMALS =
	{
    	1., 0., 0., 	1., 0., 0., 	1., 0., 0., 	1., 0., 0., 	// Face gauche (+x)
		-1., 0., 0.,	-1., 0., 0.,	-1., 0., 0.,	-1., 0., 0.,	// Face droite (-x)
		0., 1., 0.,		0., 1., 0.,		0., 1., 0.,		0., 1., 0.,		// Face dessus (+y)
		0., -1., 0.,	0., -1., 0.,	0., -1., 0.,	0., -1., 0.,	// Face dessous (-y)
		0., 0., 1.,		0., 0., 1.,		0., 0., 1.,		0., 0., 1.,		// Face arrière (+z)
		0., 0., -1.,	0., 0., -1.,	0., 0., -1.,	0., 0., -1.		// Face avant (-z)
	};

	static constexpr std::array<unsigned char, 36> INDICES =
	{
		0, 1, 2,		2, 3, 0,		// Face gauche (+x)
		4, 5, 6,		6, 7, 4,		// Face droite (-x)
		8, 9, 10,		10, 11, 8,		// Face dessus (+y)
		12, 13, 14,		14, 15, 12,		// Face dessous (-y)
		16, 17, 18,		18, 19, 16,		// Face arrière (+z)
		20, 21, 22,		22, 23, 20		// Face avant (-z)
	};

	class VBO {
	public:
		VBO(GLenum type = GL_ARRAY_BUFFER);
		~VBO();

		template <typename T, std::size_t N>
		void load(std::array<T, N> data);
		bool isLoaded() const;
		GLuint getID() const;

	private:
		GLenum m_type;
		GLuint id;
		bool loaded;
	};

private:

	static VBO cubeVBO;
	static VBO verticesBO;
	static VBO normalsBO;
	static VBO elementsBO;


public:

	/* Renvoie l'id du VBO contenant Cube::CUBE
	 * Lors du premier appel le VBO est créé et les données sont chargée
	 * dans la mémoire de la carte graphique */
	static GLuint getCubeVBO();
	/* Idem pour Cube::VERTICES */
	static GLuint getVerticesBO();
	/* Idem pour Cube::NORMALS */
	static GLuint getNormalsBO();
	/* Idem pour Cube::INDICES */
	static GLuint getElementsBO();

	/* \brief Renvoie un tableau contenant les couleurs des sommets
	 * Calcule la couleur pour chaque sommet en fonction des couleurs données par faces
	 * \param colors Tableau contenant les couleurs pour chaque face dans l'ordre +x, -x, +y, -y, +z, -z
	 * \return Tableau contenant les couleurs par sommet dans l'ordre indiqué par Cube::INDICES
	 */
	static std::array<float, 108> computeColorsByFace(std::array<sf::Color, 6> colors);

	/* \brief Renvoie un tableau contenant les coordonnées de texture des sommets
	 * Calcule les coordonnées de texture pour chaque sommet en fonction des indices de tileset données par faces
	 * \param width nombre de tiles en largeur dans le tileset
	 * \param height nombre de tiles en hauteur dans le tileset
	 * \param index Tableau contenant le numéro de la tile à afficher (sens de la lecture) pour chaque face dans l'ordre +x, -x, +y, -y, +z, -z
	 * \return Tableau contenant les coordonnées de texture par sommet dans l'ordre indiqué par Cube::INDICES
	 */
	static std::array<float, 72> computeTexCoords(unsigned width, unsigned height, std::array<unsigned, 6> const& index);


};

#endif