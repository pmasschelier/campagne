#ifndef LIGHT_HPP
#define LIGHT_HPP

#include "OGLWorld.hpp"

class Light {

public:

	Light(glm::vec3 position, glm::vec3 ambiant = glm::vec3(0.2f), glm::vec3 diffuse = glm::vec3(0.5f), glm::vec3 specular = glm::vec3(1.0));

	/* Setters pour les propriétés de la lumère */
	void setPosition(glm::vec3 position);
	void setProperties(glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular);
	
	/* Getters pour les propriétés de la lumère */
	glm::vec3 getPosition() const;
	std::tuple<glm::vec3, glm::vec3, glm::vec3> getProperties() const;

private:

	glm::vec3 m_position;

	glm::vec3 m_ambiant;
    glm::vec3 m_diffuse;
    glm::vec3 m_specular;

};

#endif