#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Input.hpp"

class Camera
{
public:

    Camera(glm::vec3 const& position = glm::vec3(0, 0, 0), 
        float horizontalAngle = 0.f,
        float verticalAngle = 0.f,
        float FoV = 45.f,
        float speed = 3.f,
        float mouseSpeed = 0.05f,
        float ratio = 4.0f / 3.0f,
        float nearClipping = 0.1f, 
        float farClipping = 100.0f);

    /* \brief Met à jour la matrice de vue
     * Calcule la nouvelle matrice de vue en fonction des entrées clavier et souris
     * et du temps écoulé depuis le dernier appel de la fonction
     * \param in objet contenant les entrées utilisateur
     * \param elapsedTime temps écoulé depuis la dernière MAJ
     */
    glm::mat4 update(Input const& in, sf::Time const& elapsedTime);

    /* Setter et getter pour la position 
     * setPosition met à jour et renvoie la matrice projection * vue
     */
    glm::mat4 setPosition(glm::vec3 position);
    glm::vec3 getPosition() const;

    /* Equivalent à setPosition(getPosition() + displacement); */
    glm::mat4 move(glm::vec3 displacement);

    /* Equivalent à setDirection(target - getPosition()); */
    glm::mat4 setTarget(glm::vec3 target);

    /* Setter et getter pour la direction
     * setDirection met à jour les angles et les vecteurs et renvoie la matrice projection * vue
     */
    glm::mat4 setDirection(glm::vec3 direction);
    glm::vec3 getDirection() const;

    /* Getters pour les vecteurs indiquand le haut et la droite de la caméra */
    glm::vec3 getRight() const;
    glm::vec3 getUp() const;
    
    /* Setters et getters pour le FoV
     * setFov met à jour la matrice projection et renvoie la matrice projection * vue
     */
    glm::mat4 setFov(float FoV);
    float getFov() const;

    /* Setters et getters pour la vitesse */
    void setSpeed(float speed);
    float getSpeed() const;

    /* Setters et getters pour la vitesse de la souris donnée en rad/px/s */
    void setMouseSpeed(float mouseSpeed);
    float getMouseSpeed() const;

    /* Setters et getters pour le ratio de la projection à l'écran
     * setRatio met à jour la matrice de projection et renvoie la matrice projection * vue
     */
    glm::mat4 setRatio(float ratio);
    float getRatio() const;

    /* Setters et getters pour la distance de clipping proche (distance en deçà de laquelle on affiche plus)
     * setNearClipping met à jour la matrice de projection et renvoie la matrice projection * vue
     */
    glm::mat4 setNearClipping(float ratio);
    float getNearClipping() const;

    /* Setters et getters pour la distance de clipping lointaine (distance au delà de laquelle on affiche plus)
     * setFarClipping met à jour la matrice de projection et renvoie la matrice projection * vue
     */
    glm::mat4 setFarClipping(float ratio);
    float getFarClipping() const;

    /* Getters respectifs pour les matrices de vue, de projection et de (projection * vue) */
    glm::mat4 getViewMatrix() const;
    glm::mat4 getProjectionMatrix() const;
    glm::mat4 getViewProjectionMatrix() const;

private:

    glm::mat4 updateProjection();
    glm::mat4 updateView();
    void updateDirectionRightUp();

    glm::vec3 m_position;
    glm::vec3 m_direction;
    glm::vec3 m_right;
    glm::vec3 m_up;
    // angle horizontal = 0.f -> regard vers +z
    float m_horizontalAngle = 3.14f;
    // angle vertical = 0.f -> regard vers l'horizon
    float m_verticalAngle;
    float m_FOV;

    float m_speed; // unités/s
    float m_mouseSpeed; // = 0.005f

    float m_ratio;
    float m_nearClipping, m_farClipping;

    glm::mat4 m_projection, m_view, m_viewProjection;
};

#endif