#ifndef PERLINNOISE_H
#define PERLINNOISE_H


#include <vector>
#include <cmath>
#include <random>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <sstream>
#include <immintrin.h>

// https://github.com/sol-prog/Perlin_Noise


using double3 = std::tuple<double, double, double>;

class PerlinNoise {
public:
	static const std::array<int, 256> ref;

public:
	// Initialize with the reference values for the permutation vector
	PerlinNoise(int octaves = 3, double frequency = 0.01f, double persistence = 0.5);
	// Generate a new permutation vector based on the value of seed
	PerlinNoise(unsigned int seed, int octaves = 3, double frequency = 0.01f, double persistence = 0.5);
	// Get a noise value, for 2D images z can have any value
	double noise(double x, double y);
	double noise(double x, double y, double z);

	double3 perlinDeriv(double x, double y);
	double3 noiseDeriv(double x, double y);
	double3 gradDeriv(int hash, double x, double y);

	double perlin(double x, double y);
	double perlin(double x, double y, double z);
private:
	double fade(double t);
	double lerp(double t, double a, double b);

	double grad(int hash, double x, double y);
	double grad(int hash, double x, double y, double z);

private:

	// The permutation vector
	std::vector<int> p;
	
	int m_octaves;
	double m_amplitude;
	double m_frequency;
	double m_persistence;

};

#endif