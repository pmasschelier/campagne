#ifndef INPUT_HPP
#define INPUT_HPP

#include "OGLWorld.hpp"

class Input
{
    public:
    
		/* \brief Constructeur de la classe
		 * \param pointeur partagé sur une fenêtre SFML
		 * \param si true la souris est replacée au centre de l'écran dès qu'elle a bougé
		 */
        Input(std::shared_ptr<sf::Window> app, bool relativeMouse = false);
        
        /* \brief Met à jour les événements
         * Si la fenêtre pointée par m_app n'existe plus la fonction lance une runtime
         * exception. Sinon la fonction vide la file d'événements, met à jour
         * terminate, mouseButtons et keyboard, ainsi que mouse et mouseRel qui correspond
         * à la position de la souris par rapport au milieu de l'écran.
         * Si la souris n'a pas bougé mouseRel = {0, 0} après la fonction.
         */
        void update();
        
        /* Renvoie terminate. */
        bool end() const;
        
        /* Renvoie true si mouseRel != {0, 0} */
        bool mouseMoved() const;
        
        /* Renvoie mouse. */
        sf::Vector2i getMouse() const;
        
        /* Renvoie mouseRel */
        sf::Vector2i getMouseRel() const;
        
        /* renvoie keyboard[k] */
        bool getKey(sf::Keyboard::Key k) const;
        
        /* keyboard[k] = on; */
        void setKey(sf::Keyboard::Key k, bool on);
        
        /* renvoie mouseButtons[b] */
        bool getButton(sf::Mouse::Button b) const;

        /* Si relativeMouse == true, la souris est bloquée au milieu de l'écran */
        void setRelativeMouse(bool relativeMouse = true);
        
    private:
    
        std::weak_ptr<sf::Window> m_app;
        sf::Event e;
        sf::Vector2i mouse;
        sf::Vector2i mouseRel;
        std::array<bool, sf::Mouse::ButtonCount> mouseButtons;
        std::array<bool, sf::Keyboard::KeyCount> keyboard;
        bool m_relativeMouse;
        bool terminate;
};

#endif
