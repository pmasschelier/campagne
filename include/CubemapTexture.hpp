#ifndef CUBEMAP_TEXTURE_HPP
#define CUBEMAP_TEXTURE_HPP

#include "OGLWorld.hpp"

class CubemapTexture
{
public:

    CubemapTexture();

    ~CubemapTexture();

    /* \brief Charge une texture cubique depuis des fichiers image
     * \param filenames Chemin vers l'image sur le disque pour chaque coté du cube dans l'ordre +x, -x, +y, -y, +z, -z
     * \return Renvoie true si tout s'est bien passé, false sinon
     */
    bool loadFromFiles(std::array<std::string, 6> const& filenames);

    /* \brief Charge une texture cubique depuis un tileset
     * \param widthTex nombre de tiles en largeur dans le tileset
     * \param heightTex nombre de tiles en hauteur dans le tileset
     * \param index Tableau contenant le numéro de la tile à afficher (sens de la lecture) pour chaque face dans l'ordre +x, -x, +y, -y, +z, -z
     * \param filename Chemin vers l'image sur le disque
     */
    bool loadFromTileMap(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index, std::string const& filename);

    /* Setters et getters pour le paramètre smooth
     * setSmooth fait des appels à glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER // GL_TEXTURE_MIN_FILTER, 
     *   GL_LINEAR // GL_LINEAR_MIPMAP_LINEAR // GL_NEAREST // GL_NEAREST_MIPMAP_LINEAR);
     */
    void setSmooth(bool smooth);
    bool isSmooth() const;

    /* Génère le mipmap pour la texture */
    void generateMipmap();

    /* Bind la texture pour un shader avec le numéro défini par GL_TEXTUREi pour i allant de 0 à 15 */
    void bind(GLenum TextureUnit = GL_TEXTURE0);

private:

    void create();

    GLuint m_id;

    bool m_smooth;
    bool m_hasMipmap;
};

#endif