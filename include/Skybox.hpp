#ifndef SKYBOX_HPP
#define SKYBOX_HPP

#include "CubemapTexture.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include "Cube.hpp"

class Skybox
{
public:
    Skybox();

    /* Setters pour la texture cubique de la skybox */
    void setCubemapTexture(std::shared_ptr<CubemapTexture> cubemapTex);

    /* \brief Charge une texture cubique pour la texture
     * Équivalent à charger une CubemapTexture avec loadFromFiles puis à appeler setCubemapTexture
     * \param filenames Chemin vers l'image sur le disque pour chaque coté du cube dans l'ordre +x, -x, +y, -y, +z, -z
     * \return Renvoie true si tout s'est bien passé, false sinon
     */
    bool loadFromFiles(std::array<std::string, 6> const& filenames);

    /* \brief Affiche la skybox sur le FBO actuellement actif
     * \param camera caméra qui doit voir la skybox
     */
    void render(Camera const& camera) const;

private:
    std::shared_ptr<CubemapTexture> m_cubemapTex;

    GLuint m_vertexbuffer;

    static Shader shader;
};

#endif