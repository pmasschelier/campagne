#ifndef SCENE_HPP
#define SCENE_HPP

#include "OGLWorld.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include "Skybox.hpp"
#include "Terrain.hpp"

class OGLScene {
public:
	OGLScene(std::shared_ptr<sf::RenderWindow> window);
	~OGLScene();
	void play();

private:

	void loadTextures();
	void draw() const;
	
private:
	sf::ContextSettings settings;
	std::shared_ptr<sf::RenderWindow> m_window;

	Camera m_camera;

	Skybox m_skybox;
	std::shared_ptr<CubemapTexture> m_skytexture;

	std::shared_ptr<PerlinNoise> m_perlin;
	std::shared_ptr<Terrain> m_terrain;
};

#endif
