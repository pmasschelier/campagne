#ifndef TERRAIN_HPP
#define TERRAIN_HPP

#include "Camera.hpp"
#include "Shader.hpp"
#include "Perlin.hpp"

class Terrain {

public:

	struct Chunk {
		GLuint vbo;
		GLuint vao;
		std::vector<std::vector<float>> heights;
	};

public:

	Terrain(std::shared_ptr<PerlinNoise> const& perlin, float maxHeight, unsigned chunkSizeX, unsigned chunkSizeZ, float dx = 1.f, float dz = 1.f);
	~Terrain();

	/* \brief Affichage du terrain dans un rayon autour de la caméra
	 * Le rayon dessine un carré de chunks autour de la caméra
	 * \param cible vers laquelle on veut réaliser le rendu
	 * \param caméra qui visualise le terrain
	 * \param rayon autour de la caméra
	 */
	void render(std::shared_ptr<sf::RenderTarget> target, Camera const& camera, unsigned radius);

private:

	void loadChunk(glm::ivec2 chunk);
	void unloadChunk(glm::ivec2 chunk);

	struct HashChunk {
		size_t operator()(glm::ivec2 chunk) const {
			return std::hash<long long>()(((long long)chunk.x)^(((long long)chunk.y) << 32));
		}
	};

	const float m_maxHeight;
	const float m_dx, m_dz;
	const unsigned m_chunkSizeX, m_chunkSizeZ;

	const std::size_t nb_vertices_per_chunk;

	std::shared_ptr<PerlinNoise> m_perlin;

	GLuint m_EBO;
	std::unordered_map<glm::ivec2, Chunk, HashChunk> m_chunks;
	std::unordered_set<glm::ivec2, HashChunk> m_chunksLoading;

	std::shared_ptr<Shader> m_shader;

	std::queue<std::thread> m_threads;
	std::mutex m_mutex;

	std::vector<double> m_loadingTimes;
};

#endif