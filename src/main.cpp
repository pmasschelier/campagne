
#include "Scene.hpp"

int main() {
	
	std::cout << "Alignement de std::array<double, 4> : " << alignof(std::array<double, 4>) <<std::endl;

    sf::ContextSettings settings;
    settings.depthBits = 24;
	settings.stencilBits = 8;
	settings.antialiasingLevel = 4;
	settings.majorVersion = 4;
	settings.minorVersion = 6;

	/* Les flags possibles sont :
	 * sf::Style::None	Aucune décoration (utile pour les splash screens, par exemple) ; ce style ne peut pas être combiné avec les autres
	 * sf::Style::Titlebar	La fenêtre possède une barre de titre
	 * sf::Style::Resize	La fenêtre peut être redimensionnée et possède un bouton de maximisation
	 * sf::Style::Close	La fenêtre possède une bouton de fermeture
	 * sf::Style::Fullscreen	La fenêtre est créée en mode plein écran; ce style ne peut pas être combiné avec les autres, et requiert un mode vidéo valide
	 * sf::Style::Default	Le style par défaut, qui est un raccourci pour Titlebar | Resize | Close 
     */

    std::shared_ptr<sf::RenderWindow> window = std::make_shared<sf::RenderWindow>(sf::VideoMode(), "Campagne", sf::Style::Fullscreen, settings);
	
	settings = window->getSettings();
	
#ifdef DEBUG
	std::cout << std::endl;
	std::cout << "depth bits:" << settings.depthBits << std::endl;
	std::cout << "stencil bits:" << settings.stencilBits << std::endl;
	std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
	std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;

	GLint v;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &v);
	std::cout << "Indice max d'un VertexAttribArray : " << v << std::endl;
#endif

	if (glewInit() != GLEW_OK) {
		std::cerr << "Échec de l'initialisation de glew" << std::endl;
        return EXIT_FAILURE;
    }

	try{
	    OGLScene scene(window);	
        scene.play();
    }
    catch(std::exception const& e){
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
