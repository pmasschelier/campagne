#include "Perlin.hpp"

const std::array<int, 256> PerlinNoise::ref = {
	151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,
    8,99,37,240,21,10,23,190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,
    35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,
    134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,
    55,46,245,40,244,102,143,54, 65,25,63,161,1,216,80,73,209,76,132,187,208, 89,
    18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,
    250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,
    189,28,42,223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 
    43,172,9,129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,
    97,228,251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,
    107,49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};


// Initialize with the reference values for the permutation vector
PerlinNoise::PerlinNoise(int octaves, double frequency, double persistence) :
    p(ref.begin(), ref.end()), m_octaves(octaves), m_frequency(frequency), m_persistence(persistence) // Initialize the permutation vector with the reference values
{
	// Duplicate the permutation vector
	p.insert(p.end(), p.begin(), p.end());
}

// Generate a new permutation vector based on the value of seed
PerlinNoise::PerlinNoise(unsigned int seed, int octaves, double frequency, double persistence) :
    m_octaves(octaves), m_frequency(frequency), m_persistence(persistence)
{
	p.resize(256);

	// Fill p with values from 0 to 255
	std::iota(p.begin(), p.end(), 0);

	// Initialize a random engine with seed
	std::default_random_engine engine(seed);

	// Suffle  using the above random engine
	std::shuffle(p.begin(), p.end(), engine);

	// Duplicate the permutation vector
	p.insert(p.end(), p.begin(), p.end());
}

double PerlinNoise::perlin(double x, double y)
{
    double r = 0.;
    double f = m_frequency;
    double amplitude = 1.0;

    for(int i = 0; i < m_octaves; i++)
    {
        r += noise(x * f, y * f) * amplitude;

        amplitude *= m_persistence;
        f *= 2;
    }

    double geo_lim = (1 - m_persistence) / (1 - amplitude);

    return r * geo_lim;
}

double3 PerlinNoise::perlinDeriv(double x, double y)
{
    double r = 0.;
    double f = m_frequency;
    double amplitude = 1.0;

	double dx = 0., dy = 0.;

    for(int i = 0; i < m_octaves; i++)
    {
		auto [dr, ddx, ddy] = noiseDeriv(x * f, y * f);
        r += dr * amplitude;
        dx += ddx * amplitude;
        dy += ddy * amplitude;

        amplitude *= m_persistence;
        f *= 2;
    }

    double geo_lim = (1 - m_persistence) / (1 - amplitude);
	r *= geo_lim;
	dx *= geo_lim;
	dy *= geo_lim;

    return std::make_tuple(r, dx, dy);
}

double PerlinNoise::perlin(double x, double y, double z)
{
    double r = 0.;
    double f = m_frequency;
    double amplitude = m_amplitude;

    for(int i = 0; i < m_octaves; i++)
    {
        r += noise(x * f, y * f, z * f) * amplitude;

        amplitude *= m_persistence;
        f *= 2;
    }

    double geo_lim = (1 - m_persistence) / (m_amplitude - amplitude);

    return r * geo_lim;
}

double PerlinNoise::noise(double x, double y) {
	// Find the unit square that contains the point
	int X = (int) floor(x) & 255;
	int Y = (int) floor(y) & 255;

	// Find relative x, y of point in square
	x -= floor(x);
	y -= floor(y);

	// Compute fade curves for each of x, y
	double u = fade(x);
	double v = fade(y);

	// Hash coordinates of the 4 square corners
	unsigned short A = p[X] + Y;
	unsigned short AA = p[A];
	unsigned short AB = p[A + 1];
	unsigned short B = p[X + 1] + Y;
	unsigned short BA = p[B];
	unsigned short BB = p[B + 1];

	// Add blended results from 8 corners of cube
	double res = lerp(v, 
                    lerp(u, grad(p[AA], x, y), grad(p[BA], x-1, y)),
                    lerp(u, grad(p[AB], x, y-1), grad(p[BB], x-1, y-1))
                );
	return res;
}

double3 PerlinNoise::noiseDeriv(double x, double y)
{
    // Calculate 2D integer coordinates i and fraction p.
    int X = (int)floor(x);
    int Y = (int)floor(y);

    x -= floor(x);
    y -= floor(y);

    // Get weights from the coordinate fraction
    double u = fade(x); // 6f^5 - 15f^4 + 10f^3
    double v = fade(y); // 6f^5 - 15f^4 + 10f^3
    // float4 w4 = float4(1, w.x, w.y, w.x * w.y);

    // Get the derivative du/dx
    double du = x * x * (x * (x * 30 - 60) + 30); // 30x^4 - 60x^3 + 30x^2
    // Get the derivative dv/dy
    double dv = y * y * (y * (y * 30 - 60) + 30); // 30y^4 - 60y^3 + 30y^2

    // Get the derivative d(u*x)/dx
    double dux = x * x * x * (x * (x * 36 - 75) + 40); // 36x^5 - 75x^4 + 40x^3
    // Get the derivative d(v*y)/dy
    double dvy = y * y * y * (y * (y * 36 - 75) + 40); // 36x^5 - 75x^4 + 40x^3

	// Hash coordinates of the 4 square corners
	unsigned short A = p[X] + Y;
	unsigned short AA = p[A];
	unsigned short AB = p[A + 1];
	unsigned short B = p[X + 1] + Y;
	unsigned short BA = p[B];
	unsigned short BB = p[B + 1];

    // Evaluate the four lattice gradients at p
	auto [gAA, dgAAx, dgAAy] = gradDeriv(p[AA], x, y);
	auto [gBA, dgBAx, dgBAy] = gradDeriv(p[BA], x-1, y);
	auto [gAB, dgABx, dgABy] = gradDeriv(p[AB], x, y-1);
	auto [gBB, dgBBx, dgBBy] = gradDeriv(p[BB], x-1, y-1);

	double n = lerp(v, lerp(u, gAA, gBA), lerp(u, gAB, gBB) );
	
    // Calculate the derivatives dn/dx and dn/dy
    float dx = dgAAx + du * (gBA - gAA) + u * (dgBAx - dgAAx) + v * (dgABx + du * (gBB - gAB) + u * (dgBBx - dgABx));
    float dy = dgAAy + u * (dgBAy - dgAAy) + dv * (gAB + u * (gBB - gAB)) + v * (dgABy + u * (dgBBy - dgABy));

    // Return the noise value, roughly normalized in the range [-1, 1]
    // Also return the pseudo dn/dx and dn/dy, scaled by the same factor
    return std::make_tuple(n, dx, dy);
}

double PerlinNoise::noise(double x, double y, double z) {
	// Find the unit cube that contains the point
	int X = (int) std::floor(x) & 255;
	int Y = (int) std::floor(y) & 255;
	int Z = (int) std::floor(z) & 255;

	// Find relative x, y,z of point in cube
	x -= std::floor(x);
	y -= std::floor(y);
	z -= std::floor(z);

	// Compute fade curves for each of x, y, z
	double u = fade(x);
	double v = fade(y);
	double w = fade(z);

	// Hash coordinates of the 8 cube corners
	int A = p[X] + Y;
	int AA = p[A] + Z;
	int AB = p[A + 1] + Z;
	int B = p[X + 1] + Y;
	int BA = p[B] + Z;
	int BB = p[B + 1] + Z;

	// Add blended results from 8 corners of cube
	double res = lerp(w, 
                    lerp(v, 
                        lerp(u, grad(p[AA], x, y, z), grad(p[BA], x-1, y, z)),
                        lerp(u, grad(p[AB], x, y-1, z), grad(p[BB], x-1, y-1, z))),
                    lerp(v, 
                        lerp(u, grad(p[AA+1], x, y, z-1), grad(p[BA+1], x-1, y, z-1)),
                        lerp(u, grad(p[AB+1], x, y-1, z-1),	grad(p[BB+1], x-1, y-1, z-1)))
                );
	return (res + 1.0)/2.0;
}

double PerlinNoise::fade(double t) { 
	return t * t * t * (t * (t * 6 - 15) + 10);
}

double PerlinNoise::lerp(double t, double a, double b) { 
	return a + t * (b - a); 
}

double PerlinNoise::grad(int hash, double x, double y) {
	int h = hash & 15;
	// Convert lower 4 bits of hash into 12 gradient directions
	double u = (h & 2) ? x : y;
	return (h & 1) ? u : -u;
}

double3 PerlinNoise::gradDeriv(int hash, double x, double y) {
	int h = hash & 15;
	// Convert lower 4 bits of hash into 12 gradient directions
	if(h & 2) {
		if(h & 1)
			return std::make_tuple(x, 1., 0.);
		else 
			return std::make_tuple(-x, -1., 0.);
	}
	else {
		if(h & 1)
			return std::make_tuple(y, 0., 1.);
		else 
			return std::make_tuple(-y, 0., -1.);
	}
}

double PerlinNoise::grad(int hash, double x, double y, double z) {
	int h = hash & 15;
	// Convert lower 4 bits of hash into 12 gradient directions
	double u = h < 8 ? x : y,
		   v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}