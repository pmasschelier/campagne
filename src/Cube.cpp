#include "Cube.hpp"

Cube::VBO::VBO(GLenum type) : m_type(type), id(0), loaded(false) {}

Cube::VBO::~VBO()
{
    if(glIsBuffer(id) == GL_TRUE)
        glDeleteBuffers(1, &id);
}

template <typename T, std::size_t N>
void Cube::VBO::load(std::array<T, N> data)
{
    if(glIsBuffer(id) == GL_TRUE)
        glDeleteBuffers(1, &id);
    
    // Génère un buffer / stock l'indice du buffer dans m_vertexbuffer
    glGenBuffers(1, &id);
	// "Bind" le buffer
	glBindBuffer(m_type, id);
	// Écrit les sommets dans le buffer

    /* Valeurs possible pour target :
	 * 	GL_ARRAY_BUFFER					Vertex attributes
	 *	GL_ATOMIC_COUNTER_BUFFER		Atomic counter storage
	 *	GL_COPY_READ_BUFFER				Buffer copy source
	 *	GL_COPY_WRITE_BUFFER			Buffer copy destination
	 *	GL_DISPATCH_INDIRECT_BUFFER		Indirect compute dispatch commands
	 *	GL_DRAW_INDIRECT_BUFFER			Indirect command arguments
	 *	GL_ELEMENT_ARRAY_BUFFER			Vertex array indices
	 *	GL_PIXEL_PACK_BUFFER			Pixel read target
	 *	GL_PIXEL_UNPACK_BUFFER			Texture data source
	 *	GL_QUERY_BUFFER					Query result buffer
	 *	GL_SHADER_STORAGE_BUFFER		Read-write storage for shaders
	 *	GL_TEXTURE_BUFFER				Texture data buffer
	 *	GL_TRANSFORM_FEEDBACK_BUFFER	Transform feedback buffer
	 *	GL_UNIFORM_BUFFER				Uniform block storage
	 * Valeurs possibles pour usage :
	 * GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY, GL_STATIC_DRAW, GL_STATIC_READ, GL_STATIC_COPY, GL_DYNAMIC_DRAW, GL_DYNAMIC_READ, ou GL_DYNAMIC_COPY
	 */
	glBufferData(m_type, data.size() * sizeof(T), &data[0], GL_STATIC_DRAW);
    glBindBuffer(m_type, 0);

    loaded = true;
}

bool Cube::VBO::isLoaded() const {
    return loaded;
}

GLuint Cube::VBO::getID() const {
    return id;
}

Cube::VBO Cube::cubeVBO;
Cube::VBO Cube::verticesBO;
Cube::VBO Cube::normalsBO;
Cube::VBO Cube::elementsBO(GL_ELEMENT_ARRAY_BUFFER);

GLuint Cube::getCubeVBO()
{
    if(!cubeVBO.isLoaded())
        cubeVBO.load(Cube::CUBE);
    return cubeVBO.getID();
}

GLuint Cube::getVerticesBO()
{
    if(!verticesBO.isLoaded())
        verticesBO.load(Cube::VERTICES);
    return verticesBO.getID();
}

GLuint Cube::getNormalsBO()
{
    if(!normalsBO.isLoaded())
        normalsBO.load(Cube::NORMALS);
    return normalsBO.getID();
}

GLuint Cube::getElementsBO()
{
    if(!elementsBO.isLoaded())
        elementsBO.load(Cube::INDICES);
    return elementsBO.getID();
}



std::array<float, 108> Cube::computeColorsByFace(std::array<sf::Color, 6> colors)
{
    std::array<float, 108> mapped_colors = std::array<float, 108>();

    for(unsigned i=0; i<6; i++) {
        for(unsigned j=0; j<6; j++) {
            mapped_colors[i*18 + j*3 + 0] = colors[i].r / 256.f;
            mapped_colors[i*18 + j*3 + 1] = colors[i].g / 256.f;
            mapped_colors[i*18 + j*3 + 2] = colors[i].b / 256.f;
        }
    }

    return mapped_colors;
}

std::array<float, 72> Cube::computeTexCoords(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index)
{
    std::array<float, 72> texCoords = std::array<float, 72>();

    float uX(1/(float)widthTex), uY(1/(float)heightTex);

    for(unsigned i=0; i<6; i++) {
        unsigned posXonTex = index[i] % widthTex;
        unsigned posYonTex = index[i] / widthTex;

        // Premier triangle (celui du bas : les deux premiers sommets sont l'arrete vers -x -y)
        texCoords[12*i + 0] = (posXonTex) * uX;
        texCoords[12*i + 1] = (posYonTex + 1) * uY;
        
        texCoords[12*i + 2] = (posXonTex + 1) * uX;
        texCoords[12*i + 3] = (posYonTex + 1) * uY;
        
        texCoords[12*i + 4] = (posXonTex + 1) * uX;
        texCoords[12*i + 5] = (posYonTex) * uY;


        // Second triangle (celui du haut : les deux premiers sommets sont l'arrete vers +x +y)
        texCoords[12*i + 6] = (posXonTex + 1) * uX;
        texCoords[12*i + 7] = (posYonTex) * uY;
        
        texCoords[12*i + 8] = (posXonTex) * uX;
        texCoords[12*i + 9] = (posYonTex) * uY;
        
        texCoords[12*i + 10] = (posXonTex) * uX;
        texCoords[12*i + 11] = (posYonTex + 1) * uY;
    }

    return texCoords;
}