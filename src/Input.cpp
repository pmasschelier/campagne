#include "Input.hpp"

Input::Input(std::shared_ptr<sf::Window> app, bool relativeMouse) :
    m_app(app), mouse(), mouseRel(), m_relativeMouse(relativeMouse), terminate(false)
{
    app->setKeyRepeatEnabled(false);
    
	keyboard.fill(false);
	mouseButtons.fill(false);
    
    if(relativeMouse)
        mouse = static_cast<sf::Vector2i>(app->getSize())/2;
    else
        mouse = sf::Mouse::getPosition(*app);
}

void Input::update() {
	std::shared_ptr<sf::Window> app = m_app.lock(); // Crée un shared_ptr à partir du weak_ptr stocké par la classe
	if(!app) // On vérifie que la fenetre existe toujours.
		throw std::runtime_error("Tentative de lire des événements dans une fenêtre qui n'existe plus.");
	
    mouseRel.x = 0;
    mouseRel.y = 0;
    while(app->pollEvent(e)) { // Tant qu'il y a des évenements à traiter
        switch(e.type) {
        case sf::Event::Closed:
            terminate = true;
            break;
        case sf::Event::KeyPressed:
            keyboard[e.key.code] = true;
            break;
        case sf::Event::KeyReleased:
            keyboard[e.key.code] = false;
            break;
        case sf::Event::MouseButtonPressed:
            mouseButtons[e.mouseButton.button] = true;
            break;
        case sf::Event::MouseButtonReleased:
            mouseButtons[e.mouseButton.button] = false;
            break;
        case sf::Event::MouseMoved:
            mouseRel.x = e.mouseMove.x - app->getSize().x/2;
            mouseRel.y = e.mouseMove.y - app->getSize().y/2;
            mouse.x    = e.mouseMove.x;
            mouse.y    = e.mouseMove.y;
            break;
        default:
            break;
        }
    }
    if(m_relativeMouse)
        sf::Mouse::setPosition(static_cast<sf::Vector2i>(app->getSize())/2, *app); // On replace la souris au centre de l'écran
}

bool Input::end() const {
    return terminate;
}
bool Input::mouseMoved() const {
    return mouseRel.x || mouseRel.y;
}
sf::Vector2i Input::getMouse() const{
    return mouse;
}
sf::Vector2i Input::getMouseRel() const{
    return mouseRel;
}
bool Input::getKey(sf::Keyboard::Key k) const{
    return keyboard[k];
}
void Input::setKey(sf::Keyboard::Key k, bool on) {
    keyboard[k] = on;
}
bool Input::getButton(sf::Mouse::Button b) const{
    return mouseButtons[b];
}

void Input::setRelativeMouse(bool relativeMouse){
    m_relativeMouse = relativeMouse;
}