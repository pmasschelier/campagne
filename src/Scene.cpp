#include "Scene.hpp"


OGLScene::OGLScene(std::shared_ptr<sf::RenderWindow> window) : 
	m_window(window),
	m_camera(
		glm::vec3(-24, 18, -18), // Position de la caméra
		M_PI/4, // horizontalAngle
		-M_PI/5, // verticalAngle
		45.f, // FoV
		50.f, // speed
		0.05f, // mouse speed
		4.f / 3.f, // ratio
		0.1f, // near clipping
		1000.f // far clipping
	),
	m_skytexture(std::make_shared<CubemapTexture>()),
	m_perlin(std::make_shared<PerlinNoise>(8, 0.01f, 0.5)),
	m_terrain(std::make_shared<Terrain>(m_perlin, 200.f, 100, 100, 2.f, 2.f))
{
	m_camera.setRatio((float)m_window->getSize().x / (float) m_window->getSize().y);

	m_window->setActive(true);

	glEnable(GL_DEPTH_TEST);
	// Accepte le fragment s'il est plus proche de la caméra que l'ancien
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE); // Par défaut : glCullFace(GL_CCW);
	
	loadTextures();
}

OGLScene::~OGLScene()
{

}

void OGLScene::loadTextures()
{
	const std::array<std::string, 6> filenames = {
		"ressources/skybox/skybox_px.jpg",
		"ressources/skybox/skybox_nx.jpg",
		"ressources/skybox/skybox_py.jpg",
		"ressources/skybox/skybox_ny.jpg",
		"ressources/skybox/skybox_pz.jpg",
		"ressources/skybox/skybox_nz.jpg",
	};
	m_skytexture->loadFromFiles(filenames);
	m_skybox.setCubemapTexture(m_skytexture);
	
}

void OGLScene::draw() const
{
	m_terrain->render(m_window, m_camera, 2);
	m_skybox.render(m_camera);
}

void OGLScene::play()
{
    Input in(m_window, true);

	bool wireframe(false), paused(false);
	sf::Clock loopclock;
	sf::Time lastTime(loopclock.getElapsedTime()), newTime, elapsedTime;

    while (!in.end())
    {
        in.update();

		if(in.getKey(sf::Keyboard::W)) {
			if(!wireframe)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			wireframe = !wireframe;
			in.setKey(sf::Keyboard::W, false);
		}

		if(in.getKey(sf::Keyboard::P)) {
			in.setRelativeMouse(paused);
			paused = !paused;
			in.setKey(sf::Keyboard::P, false);
		}

		if(!paused) {
			newTime = loopclock.getElapsedTime();
			elapsedTime = newTime - lastTime;

			m_camera.update(in, elapsedTime);

			lastTime = newTime;
		}
        
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		draw();

		m_window->display();

        sf::sleep(sf::milliseconds(25));
    }

}
