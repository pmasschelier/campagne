#include "Camera.hpp"

Camera::Camera(glm::vec3 const& position, float horizontalAngle, float verticalAngle, float FoV, float speed, float mouseSpeed, float ratio, float nearClipping, float farClipping) :
    m_position(position),
    m_direction(cos(verticalAngle) * sin(horizontalAngle), sin(verticalAngle), cos(verticalAngle) * cos(horizontalAngle)),
    m_right(sin(horizontalAngle - M_PI/2.0f), 0, cos(horizontalAngle - M_PI/2.0f)),
    m_up(glm::cross(m_right, m_direction)),
    m_horizontalAngle(horizontalAngle),
    m_verticalAngle(verticalAngle),
    m_FOV(FoV),
    m_speed(speed),
    m_mouseSpeed(mouseSpeed),
    m_ratio(ratio),
    m_nearClipping(nearClipping),
    m_farClipping(farClipping),
    m_projection(glm::perspective(glm::radians(m_FOV), m_ratio, m_nearClipping, m_farClipping)),
    m_view(glm::lookAt(m_position, m_position+m_direction, m_up)),
    m_viewProjection(m_projection * m_view)
{

}

glm::mat4 Camera::updateProjection()
{
    m_projection = glm::perspective(glm::radians(m_FOV), m_ratio, m_nearClipping, m_farClipping);
    m_viewProjection = m_projection * m_view;
    return m_viewProjection;
}

glm::mat4 Camera::updateView()
{
    m_view = glm::lookAt(m_position, m_position+m_direction, m_up);
    m_viewProjection = m_projection * m_view;
    return m_viewProjection;
}

void Camera::updateDirectionRightUp()
{
    m_direction = glm::vec3(
            cos(m_verticalAngle) * sin(m_horizontalAngle),
            sin(m_verticalAngle),
            cos(m_verticalAngle) * cos(m_horizontalAngle)
        );
    
    m_right = glm::vec3(
        sin(m_horizontalAngle - 3.14f/2.0f),
        0,
        cos(m_horizontalAngle - 3.14f/2.0f)
    );

    m_up = glm::cross( m_right, m_direction );
}

glm::mat4 Camera::update(Input const& in, sf::Time const& elapsedTime)
{
    bool viewChanged(false);

    if(in.mouseMoved())
    {
        m_horizontalAngle -= m_mouseSpeed * elapsedTime.asSeconds() * in.getMouseRel().x;
        m_verticalAngle   -= m_mouseSpeed * elapsedTime.asSeconds() * in.getMouseRel().y;

        const float lim_angle = 19./40. * M_PI;
        if(m_verticalAngle < -lim_angle) m_verticalAngle = -lim_angle;
        if(m_verticalAngle > lim_angle) m_verticalAngle = lim_angle;

        updateDirectionRightUp();

        viewChanged = true;
    }

    if(in.getKey(sf::Keyboard::Up)) {
        m_position += m_speed * elapsedTime.asSeconds() * m_direction;
        viewChanged = true;
    }
    if(in.getKey(sf::Keyboard::Down)) {
        m_position -= m_speed * elapsedTime.asSeconds() * m_direction;
        viewChanged = true;
    }
    if(in.getKey(sf::Keyboard::Right)) {
        m_position += m_speed * elapsedTime.asSeconds() * m_right;
        viewChanged = true;
    }
    if(in.getKey(sf::Keyboard::Left)) {
        m_position -= m_speed * elapsedTime.asSeconds() * m_right;
        viewChanged = true;
    }

    if(viewChanged) 
        updateView();
    
    return m_viewProjection;
}

glm::mat4 Camera::setPosition(glm::vec3 position) {
    m_position = position;
    return updateView();
}

glm::vec3 Camera::getPosition() const {
    return m_position;
}

glm::mat4 Camera::move(glm::vec3 displacement) {
    return setPosition(m_position + displacement);
}

glm::mat4 Camera::setTarget(glm::vec3 target) {
    return setDirection(target - m_position);
}

glm::mat4 Camera::setDirection(glm::vec3 direction) 
{
    m_direction = direction;
    float norm = glm::length(m_direction);
    m_verticalAngle = asin(m_direction.y/norm);
    m_horizontalAngle = acos(m_direction.z/norm);
    if(m_direction.x < 0)
        m_horizontalAngle = -m_horizontalAngle;
    updateDirectionRightUp();
    return updateView();

}
glm::vec3 Camera::getDirection() const {
    return m_direction;
}

glm::vec3 Camera::getRight() const {
    return m_right;
}

glm::vec3 Camera::getUp() const {
    return m_up;
}

glm::mat4 Camera::setFov(float FoV) {
    m_FOV = FoV;
    return updateProjection();
}

float Camera::getFov() const {
    return m_FOV;
}

void Camera::setSpeed(float speed) {
    m_speed = speed;
}

float Camera::getSpeed() const {
    return m_speed;
}

void Camera::setMouseSpeed(float mouseSpeed) {
    m_mouseSpeed = mouseSpeed;
}

float Camera::getMouseSpeed() const {
    return m_mouseSpeed;
}

glm::mat4 Camera::setRatio(float ratio) {
    m_ratio = ratio;
    return updateProjection();
}

float Camera::getRatio() const {
    return m_ratio;
}

glm::mat4 Camera::setNearClipping(float nearClipping) {
    m_nearClipping = nearClipping;
    return updateProjection();
}

float Camera::getNearClipping() const {
    return m_nearClipping;
}

glm::mat4 Camera::setFarClipping(float farClipping) {
    m_farClipping = farClipping;
    return updateProjection();
}

float Camera::getFarClipping() const
{
    return m_farClipping;
}

glm::mat4 Camera::getViewMatrix() const {
    return m_view;
}

glm::mat4 Camera::getProjectionMatrix() const {
    return m_projection;
}

glm::mat4 Camera::getViewProjectionMatrix() const {
    return m_viewProjection;
}