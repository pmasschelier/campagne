#include "Light.hpp"

Light::Light(glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular) :
	m_position(position), m_ambiant(ambiant), m_diffuse(diffuse), m_specular(specular)
{

}

void Light::setPosition(glm::vec3 position) {
	m_position = position;
}

void Light::setProperties(glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular) {
	m_ambiant = ambiant;
	m_diffuse = diffuse;
	m_specular = specular;
}

glm::vec3 Light::getPosition() const {
	return m_position;
}

std::tuple<glm::vec3, glm::vec3, glm::vec3> Light::getProperties() const {
	return {m_ambiant, m_diffuse, m_specular};
}