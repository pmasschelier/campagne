#include "Terrain.hpp"

Terrain::Terrain(std::shared_ptr<PerlinNoise> const& perlin, float maxHeight, unsigned chunkSizeX, unsigned chunkSizeZ, float dx, float dz) :
	m_maxHeight(maxHeight),
	m_dx(dx), m_dz(dz),
	m_chunkSizeX(chunkSizeX),
	m_chunkSizeZ(chunkSizeZ),
	nb_vertices_per_chunk(chunkSizeX * chunkSizeZ * 6),
	m_perlin(perlin),
	m_shader(std::make_shared<Shader>())
{
	std::vector<unsigned> indices;
	indices.resize(nb_vertices_per_chunk);

	unsigned n = 0;
	for(unsigned i=0; i < m_chunkSizeX; i++) {
		for(unsigned j=0; j < m_chunkSizeZ; j++) {

			indices[n++] = i * (m_chunkSizeZ + 1) + j;
			indices[n++] = i * (m_chunkSizeZ + 1) + j + 1;
			indices[n++] = (i + 1) * (m_chunkSizeZ + 1) + j;

			indices[n++] = i * (m_chunkSizeZ + 1) + j + 1;
			indices[n++] = (i + 1) * (m_chunkSizeZ + 1) + j + 1;
			indices[n++] = (i + 1) * (m_chunkSizeZ + 1) + j;
		}
	}
	assert(n == nb_vertices_per_chunk);

	// Génère un buffer / stock l'indice du buffer dans m_vertexbuffer
    glGenBuffers(1, &m_EBO);
	// "Bind" le buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	// Écrit les sommets dans le buffer
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned), &indices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	m_shader->addFile("ressources/shaders/terrain.vert", GL_VERTEX_SHADER);
	m_shader->addFile("ressources/shaders/terrain.frag", GL_FRAGMENT_SHADER);
	m_shader->load();
}

Terrain::~Terrain()
{
	while(!m_threads.empty()) {
		m_threads.front().join();
		m_threads.pop();
	}
	if(glIsBuffer(m_EBO) == GL_TRUE)
        glDeleteBuffers(1, &m_EBO);
	while(m_chunks.begin() != m_chunks.end()) {
		unloadChunk(m_chunks.begin()->first);
	}
	std::cout << "Average chunk loading time : " << std::accumulate(m_loadingTimes.begin(), m_loadingTimes.end(), 0.) / m_loadingTimes.size() << "ms" << std::endl;
}

void Terrain::loadChunk(glm::ivec2 chunk)
{
	if(m_chunks.count(chunk) > 0) {
		std::cout << "Chunk (" << chunk.x << ", " << chunk.y << ") already loaded" << std::endl;
		return;
	}

	std::cout << "Loading chunk (" << chunk.x << ", " << chunk.y << ")" << std::endl;

	sf::Clock chrono;

	Chunk ch;

	const unsigned nb_vertices = (m_chunkSizeX + 1) * (m_chunkSizeZ + 1);
	ch.heights.resize(m_chunkSizeX + 1);
	for(auto& v : ch.heights)
		v.resize(m_chunkSizeZ + 1);
	std::vector<float> vertices(nb_vertices * 3);
	std::vector<float> normals(nb_vertices * 3);

	unsigned n = 0;
	const double offsetX = chunk.x * (double)m_chunkSizeX * (double)m_dx;
	const double offsetZ = chunk.y * (double)m_chunkSizeZ * (double)m_dz;

	for(unsigned i=0; i < m_chunkSizeX + 1; i++) {
		for(unsigned j=0; j < m_chunkSizeZ + 1; j++) {
			double X = chunk.x * (double)m_chunkSizeX + (double)i;
			double Y = chunk.y * (double)m_chunkSizeZ + (double)j;
			auto [val, dx, dz] =  m_perlin->perlinDeriv(X, Y);
			val *= m_maxHeight;
			dx *= m_maxHeight;
			dz *= m_maxHeight;
			vertices[n++] = i * m_dx + offsetX;
			normals[n] = -dx;
			vertices[n++] = val;
			normals[n] = 1;
			vertices[n++] = j * m_dz + offsetZ;
			normals[n] = -dz;
			ch.heights[i][j] = val; 
		}
	}
	assert(n == nb_vertices * 3);

	m_mutex.lock();
	glGenBuffers(1, &ch.vbo);
	// "Bind" le buffer
	glBindBuffer(GL_ARRAY_BUFFER, ch.vbo);
	// std::cout << "génération du VB0 : " << m_chunks[chunk].vao << std::endl;
	
	// Écrit les sommets dans le buffer
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Génération d'un VAO
	glGenVertexArrays(1, &ch.vao);
	// std::cout << "génération du VA0 : " << m_chunks[chunk].vao << std::endl;

	// Remplissage du VAO
	glBindVertexArray(ch.vao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);

		glBindBuffer(GL_ARRAY_BUFFER, ch.vbo);
		glVertexAttribPointer(
			0,                  // Numéro de l'attribut (doit correspondre dans le shader)
			3,                  // taille du tableau
			GL_FLOAT,           // type des éléments
			GL_FALSE,           // normalized?
			0,                  // stride, 0 => les éléments sont considérés comme juxtaposés dans le tableau
			(void*)0            // offset
			);
		glEnableVertexAttribArray(0);

		/* glBindBuffer(GL_ARRAY_BUFFER, m_normalsbuffer);
		glVertexAttribPointer(
			1,                  // Numéro de l'attribut (doit correspondre dans le shader)
			3,                  // taille du tableau
			GL_FLOAT,           // type des éléments
			GL_FALSE,           // normalized?
			0,                  // stride, 0 => les éléments sont considérés comme juxtaposés dans le tableau
			(void*)0            // offset
			);
		glEnableVertexAttribArray(1); */

	glBindVertexArray(0);

	m_chunks[chunk] = ch;
	m_mutex.unlock();

	double loadingTime = chrono.getElapsedTime().asMicroseconds() / 1000.f;
	m_loadingTimes.push_back(loadingTime);
	std::cout << "Loading of chunk n°(" << chunk.x << ", " << chunk.y << ") finished in " << loadingTime << "ms." << std::endl;
	std::cout << "VAO : " << m_chunks[chunk].vao << std::endl;
}

void Terrain::unloadChunk(glm::ivec2 chunk) {
	if(m_chunks.count(chunk) == 0)
		return;
	std::cout << "Unloading chunk (" << chunk.x << ", " << chunk.y << ")" << std::endl;

	glDeleteBuffers(1, &m_chunks[chunk].vbo);
	glDeleteVertexArrays(1, &m_chunks[chunk].vao);

	m_chunks.erase(chunk);
}

void Terrain::render(std::shared_ptr<sf::RenderTarget> target, Camera const& camera, unsigned radius)
{
	glm::vec3 pos = camera.getPosition();
	const int chunkX = pos.x / (m_dx * (float)m_chunkSizeX) - ((pos.x < 0) ? 1 : 0);
	const int chunkZ = pos.z / (m_dz * (float)m_chunkSizeZ) - ((pos.z < 0) ? 1 : 0);
	
	// Utilise le shader
	glUseProgram(m_shader->getID());
	m_shader->set("modelViewProjectionMatrix", camera.getViewProjectionMatrix());
	
	for(int x = -radius; x <= (int)radius; x++) {
		for(int y = -radius; y <= (int)radius; y++) {
			const glm::ivec2 chunk(chunkX + x, chunkZ + y);
			if(m_chunks.count(chunk) > 0) {
				glBindVertexArray(m_chunks[chunk].vao);
				glDrawElements(GL_TRIANGLES, nb_vertices_per_chunk, GL_UNSIGNED_INT, 0);
			}
			else if(m_chunksLoading.count(chunk) == 0) {
				m_chunksLoading.emplace(chunk);
				m_threads.emplace([this, chunk, target]() {target->setActive(); this->loadChunk(chunk); });
			}
		}
	}
	
	glBindVertexArray(0);
}